import React from "react";
import { render } from "react-dom";

import { createStore, applyMiddleware, combineReducers } from "redux";
import { Provider } from "react-redux";

import logger from "redux-logger";

import App from "./containers/App";
import reducer from "./reducer/";

// const ADD_TODO = "@@wbc/todos/add";


const store = createStore(
  combineReducers({ reducer }),
  applyMiddleware(logger)
);

// store.subscribe(() => {
//   console.log("State" + store.getState());
// });

/* store.dispatch({ type: "ADD_TODO", value: 20 }); */
render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("app")
);
