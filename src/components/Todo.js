import React from "react";

export const Todos = props => {
  return <div>
      <h2>List des tâches</h2>
      <ul>
        {props.todos.map((todo, index) => <li key={index}>
            {" "}
            {todo.desc}
            <br />
            <button onClick={() => {
                props.removeTodo(index);
              }}>
              Delete X
            </button>
            {"  "}
            <button onClick={() => {
                props.markAsDone(index);
              }}>
              Completed
            </button>
          </li>)}
      </ul>
      <button onClick={() => {
          props.markAllAsDone();
        }}>
        MARK ALL AS DONE
      </button>
      
    </div>;
};
