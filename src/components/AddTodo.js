import React from "react";

export const AddTodo = props => {
  return <div>
      <h2>Création d'une tâche</h2>
      <form onSubmit={props.submitForm}>
        <textarea name="" id="" cols="30" rows="10" placeholder="Todo description" name="desc" />
        <br />
        <br />
        <input type="submit" value="Add" />
      </form>
      <hr />
    </div>;
};
