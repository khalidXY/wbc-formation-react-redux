export const ADD_TODO = "@@wbc/todos/add";
export const REMOVE_TODO = "@@wbc/todos/remove";
export const MARK_AS_DONE = "@@wbc/todos/markAsDone";
export const MARK_ALL_AS_DONE = "@@wbc/todos/markAllAsDone";
