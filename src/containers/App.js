import React, { Component } from "react";
import { connect } from "react-redux";


import { Todos } from "../components/Todo";
import {AddTodo} from "../components/AddTodo";

import {ADD_TODO, REMOVE_TODO, MARK_AS_DONE, MARK_ALL_AS_DONE} from '../Action';

class App extends Component {
  constructor(props) {
    super(props);
  }

  submitForm(e) {
    e.preventDefault();


    // GET PARAMS
    let todo = {};
    todo.desc = e.target.desc.value;
    todo.completed = false;

    //  RESET FORM
    e.target.desc.value = "";


    // PASS DATA 
    this.props.submitForm(todo);
  }


  render() {
    return <div>
        <AddTodo submitForm={this.submitForm.bind(this)} />
        <Todos todos={this.props.todos} removeTodo={this.props.removeTodo} markAsDone={this.props.markAsDone} markAllAsDone={this.props.markAllAsDone} />
      </div>;
  }
}

const mapStateToProps = state => {
  return { todos: state.reducer.todos };
};

const mapDispatchToProps = dispatch => {
  return {
     submitForm: todo => {
      dispatch({ type: ADD_TODO, todo });
    },
     removeTodo: index => {
      dispatch({ type: REMOVE_TODO, index });
    },
    markAsDone: index => {
      dispatch({ type: MARK_AS_DONE, index });
    },

    markAllAsDone: () => {
      dispatch({ type: MARK_ALL_AS_DONE });
    },


    
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
