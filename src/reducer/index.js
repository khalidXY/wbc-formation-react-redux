import {
  ADD_TODO,
  REMOVE_TODO,
  MARK_AS_DONE,
  MARK_ALL_AS_DONE
} from "../Action";



const initialState = {
  todos: [
    {
      desc: "Iure quo sit omnis necessitatibus minima inventore",
      completed: true
    },
    {
      desc: "Iure quo sit omnis necessitatibus minima inventore",
      completed: false
    },
    {
      desc: "Iure quo sit omnis necessitatibus minima inventore",
      completed: false
    }
  ]
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ADD_TODO: {
      return { ...state, todos: [action.todo, ...state.todos] };
      break;
    }
    case REMOVE_TODO: {
      return { ...state, todos: state.todos.filter((todo, index) => {
          return index != action.index;
        }) };

      break;
    }
    case MARK_AS_DONE: {
      return { todos: state.todos.map((todo, index) => {
          return index == action.index ? { ...todo, completed: !todo.completed } : todo;
        }) };
      break;
    }
    case MARK_ALL_AS_DONE: {
      return { todos: state.todos.map((todo) => {
          return  { ...todo, completed: true };
        }) };
      break;
    }

    default:
      return state;
  }
};
